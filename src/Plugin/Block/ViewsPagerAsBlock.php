<?php

namespace Drupal\views_pager_as_block\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\views\Views;

/**
 * Provides a 'Views pager' block.
 *
 * @Block(
 *  id = "views_pager",
 *  admin_label = @Translation("Views Pager"),
 * )
 */
class ViewsPagerAsBlock extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\Core\Routing\RouteMatchInterface definition.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = new static($configuration, $plugin_id, $plugin_definition);
    $instance->routeMatch = $container->get('current_route_match');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {

    // Current page is a view ?
    $view_id = $this->routeMatch->getParameter('view_id');
    if (!empty($view_id)) {
      // Get current route
      $route = $this->routeMatch->getRouteObject();

      // Get view id and display id from route.
      $view_id = $route->getDefault('view_id');
      $display_id = $route->getDefault('display_id');
      if (!empty($view_id) && !empty($display_id)) {
        // Get the view by id.
        $view = Views::getView($view_id);
        if ($view) {
          // Set display id.
          $view->setDisplay($display_id);

          // Handle arguments, remove useless ones.
          $arguments = $this->routeMatch->getParameters()->all();
          unset($arguments['view_id']);
          unset($arguments['display_id']);
          $view->setArguments($arguments);

          // Execute view
          $view->execute();

          // Render pager
          $pager = $view->getPager();
          $input = $view->getExposedInput();

          return [
            'pager' => $pager->render($input)
          ];
        }
      }
    }
  }

}
